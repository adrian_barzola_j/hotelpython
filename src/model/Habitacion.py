from controller.HabitacionControlador import HabitacionControlador

class Habitacion:
    def __init__(self
           ,codigo_piso = None
           ,numero_habitacion = None
           ,tipo_habitacion = None
           ,numero_camas = None
           ,banio = None
           ,ventana = None
           ,aire_acondicionado = None
           ,ventilador = None
           ,recibidor = None
           ,armario = None
           ,minibar = None
           ,estado = None
           ,ultimo_hospedaje = None
        ):
        self._codigo_piso = codigo_piso
        self._numero_habitacion = numero_habitacion
        self._tipo_habitacion = tipo_habitacion
        self._numero_camas = numero_camas
        self._banio = banio
        self._ventana = ventana
        self._aire_acondicionado = aire_acondicionado
        self._ventilador = ventilador
        self._recibidor = recibidor
        self._armario = armario
        self._minibar = minibar
        self._estado = estado
        self._ultimo_hospedaje = ultimo_hospedaje

        self._controlador = HabitacionControlador()
    
    def insertar(self):
        return self._controlador.insertar(self)

    def actualizar(self):
        return self._controlador.actualizar(self)

    def eliminar(self):
        return self._controlador.eliminar(self)
    
    def consultar_uno(self, codigo_piso, no_habitacion):
        return self._controlador.consultar_uno(codigo_piso, no_habitacion)

    def consultar_muchos(self):
        return self._controlador.consultar_muchos()
