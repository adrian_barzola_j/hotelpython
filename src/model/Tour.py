from controller.TourControlador import TourControlador

class Tour:
    def __init__(self,
                id_tour = None,
                nombre = None,
                duracion = None,
                costo = None,
                estado = None,
                resumen = None,
                imagen_url = None
            ):
            self._id_tour = id_tour
            self._nombre = nombre
            self._duracion = duracion
            self._costo = costo
            self._estado = estado
            self._resumen = resumen
            self._imagen_url = imagen_url

            self._controlador = TourControlador()
    
    def insertar(self):
        self._controlador.insertar(self)

    def actualizar(self):
        self._controlador.actualizar(self)

    def eliminar(self):
        self._controlador.eliminar(self)
    
    def consultar_uno(self, id_tour):
        return self._controlador.consultar_uno(id_tour)
    
    def consultar_nombre(self, nombre):
        return self._controlador.consultar_nombre(nombre)

    def consultar_muchos(self):
        return self._controlador.consultar_muchos()
