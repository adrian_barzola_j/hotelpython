from controller.ClienteControlador import ClienteControlador
class Cliente:
    def __init__(self, 
            id_cliente = None, 
            identificacion = None,
            nombre = None,
            telefono = None,
            direccion = None,
            fecha_nacimiento = None,
            email = None):
        self._id_cliente = id_cliente
        self._identificacion = identificacion
        self._nombre = nombre
        self._telefono = telefono
        self._direccion = direccion
        self._fecha_nacimiento = fecha_nacimiento
        self._email = email

        self._controlador = ClienteControlador()
    
    def insertar(self):
        return self._controlador.insertar(self)

    def actualizar(self):
        return self._controlador.actualizar(self)

    def eliminar(self):
        return self._controlador.eliminar(self)
    
    def consultar_uno(self, id_cliente):
        return self._controlador.consultar_uno(id_cliente)
    
    def consulta_identificacion(self, identificacion):
        return self._controlador.consultar_identificacion(identificacion)

    def consultar_muchos(self, por_id_reserva = None):
        return self._controlador._consultar_muchos(por_id_reserva)
    