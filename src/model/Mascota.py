from controller.MascotaControlador import MascotaControlador
class Mascota:
    def __init__(self,
            id_mascota =None,
            id_reserva =None,
            nombre = None,
            tipo = None,
            descripcion =None,
            cuidados_especiales = None):
        self._id_mascota = id_mascota
        self._id_reserva = id_reserva
        self._nombre = nombre
        self._tipo = tipo
        self._descripcion = descripcion
        self._cuidados_especiales = cuidados_especiales

        self._controlador = MascotaControlador()
    
    def insertar(self):
        return self._controlador.insertar(self)

    def actualizar(self):
        return self._controlador.actualizar(self)

    def eliminar(self):
        return self._controlador.eliminar(self)
    
    def consultar_uno(self, id_mascota):
        return self._controlador.consultar_uno(id_mascota)

    def consultar_muchos(self, por_id_reserva = None, 
                               por_tipo = None):
        return self._controlador._consultar_muchos(por_id_reserva, por_tipo)