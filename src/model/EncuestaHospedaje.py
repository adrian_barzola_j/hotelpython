class EncuestaHospedaje:
        def __init__(self,
                    id_encuesta_hospedaje = None,
                    id_reserva = None,
                    id_cliente = None,
                    respuesta_atencion = None,
                    respuesta_limpieza = None,
                    respuesta_comida = None,
                    respuesta_ubicacion = None,
                    comentarios = None,
                    fecha_encuesta = None):
            self._id_encuesta_hospedaje = id_encuesta_hospedaje
            self._id_reserva = id_reserva
            self._id_cliente = id_cliente
            self._respuesta_atencion = respuesta_atencion
            self._respuesta_limpieza = respuesta_limpieza
            self._respuesta_comida = respuesta_comida
            self._respuesta_ubicacion = respuesta_ubicacion
            self._comentarios = comentarios
            self._fecha_encuesta = None