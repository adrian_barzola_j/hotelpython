from controller.ServicioAdicionalrControlador import ServicioAdicionalControlador

class ServicioAdicional:
    def __init__(self,
                id_servicio_adicional = None,
                descripcion = None,
                costo = None,
                estado = None,
                membresia = None,
                resumen = None,
                imagen_url = None):
        self._id_servicio_adicional = id_servicio_adicional
        self._descripcion = descripcion
        self._costo = costo
        self._estado = estado
        self._membresia = membresia
        self._resumen = resumen
        self._imagen_url = imagen_url

        self._controlador = ServicioAdicionalControlador()


    def insertar(self):
        return self._controlador.insertar(self)

    def actualizar(self):
        return self._controlador.actualizar(self)

    def eliminar(self):
        return self._controlador.eliminar(self)
    
    def consultar_uno(self, id_tour):
        return self._controlador.consultar_uno(id_tour)
    
    def consultar_nombre(self, nombre_tour):
        return self._controlador.consultar_nombre(nombre_tour)

    def consultar_muchos(self, por_nombre = None, 
                               por_estado = None):
        return self._controlador.consultar_muchos()