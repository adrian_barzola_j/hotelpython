from controller.ReservaControlador import ReservaControlador
class Reserva:
    def __init__(self,
                id_reserva = None,
                id_cliente = None,
                fecha_llegada = None,
                fecha_salida = None,
                no_adultos = None,
                no_ninos = None,
                nombre_tarjeta = None,
                numero_tarjeta = None,
                tipo_tarjeta = None,
                fecha_expiracion = None,
                cvv = None,
                total_pagar = None,
                estado = None):
        self._id_reserva = id_reserva
        self._id_cliente = id_cliente
        self._fecha_llegada = fecha_llegada
        self._fecha_salida = fecha_salida
        self._no_adultos = no_adultos
        self._no_ninos = no_ninos
        self._nombre_tarjeta = nombre_tarjeta
        self._numero_tarjeta = numero_tarjeta
        self._tipo_tarjeta = tipo_tarjeta
        self._fecha_expiracion = fecha_expiracion
        self._cvv = cvv
        self._total_pagar = total_pagar
        self._estado = estado

        self._controlador = ReservaControlador()
    
    def insertar(self):
        return self._controlador.insertar(self)

    def actualizar(self):
        return self._controlador.actualizar(self)

    def eliminar(self):
        return self._controlador.eliminar(self)
    
    def consultar_uno(self, id_cliente):
        return self._controlador.consultar_uno(id_cliente)
    
    def consulta_identificacion(self, identificacion):
        return self._controlador.consultar_identificacion(identificacion)

    def consultar_muchos(self, por_id_reserva = None):
        return self._controlador._consultar_muchos(por_id_reserva)