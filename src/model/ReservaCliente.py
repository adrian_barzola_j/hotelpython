from controller.ReservaClienteControlador import ReservaClienteControlador
class ReservaCliente:
    def __init__(self,
                id_reserva_cliente = None,
                id_reserva = None,
                id_cliente = None,
                no_maletas = None,
                fecha_ingreso = None,
                fecha_salida = None,
                observacion = None):
        self._id_reserva_cliente = id_reserva_cliente
        self._id_reserva = id_reserva
        self._id_cliente = id_cliente
        self._no_maletas = no_maletas
        self._fecha_ingreso = fecha_ingreso
        self._fecha_salida = fecha_salida
        self._observacion = observacion

        self._controlador = ReservaClienteControlador()
    
    def insertar(self):
        return self._controlador.insertar(self)

    def actualizar(self):
        return self._controlador.actualizar(self)

    def eliminar(self):
        return self._controlador.eliminar(self)
    
    def consultar_uno(self, id_reserva_cliente):
        return self._controlador.consultar_uno(id_reserva_cliente)

    def consultar_muchos(self, id_reserva = None):
        return self._controlador.consultar_muchos(id_reserva)