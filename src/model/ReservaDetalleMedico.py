from controller.ReservaDetalleMedicoControlador import ReservaDetalleMedicoControlador
class ReservaDetalleMedico:
    def __init__(self,
                id_reserva_detalle_medico = None,
                id_reserva = None,
                id_cliente = None,
                peso = None,
                raza = None,
                tipo_sangre = None,
                sexo = None,
                estatura = None,
                enfermedades = None,
                alergias = None,
                condiciones_especiales = None
                ):
        self._id_reserva_detalle_medico = id_reserva_detalle_medico
        self._id_reserva = id_reserva
        self._id_cliente = id_cliente
        self._peso = peso
        self._raza = raza
        self._tipo_sangre = tipo_sangre
        self._sexo = sexo
        self._estatura = estatura
        self._enfermedades = enfermedades
        self._alergias = alergias
        self._condiciones_especiales = condiciones_especiales

        self._controlador = ReservaDetalleMedicoControlador()
    
    def insertar(self):
        return self._controlador.insertar(self)

    def actualizar(self):
        return self._controlador.actualizar(self)

    def eliminar(self):
        return self._controlador.eliminar(self)
    
    def consultar_uno(self, id_reserva_detalle_medico, id_reserva):
        return self._controlador.consultar_uno(id_reserva_detalle_medico, id_reserva)

    def consultar_muchos(self, por_id_reserva = None):
        return self._controlador._consultar_muchos(por_id_reserva)
