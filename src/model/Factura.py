class Factura:
       
    def __init__(self, id_factura=None, 
                    fecha_emision= None, 
                    id_cliente=None, 
                    subtotal_0 = None, 
                    subtotal_12 = None, 
                    iva = None
                    ):
        self._id_factura = id_factura
        self._fecha_emision = fecha_emision
        self._id_cliente = id_cliente
        self._subtotal_0 = subtotal_0
        self._subtotal_12 = subtotal_12
        self._iva = iva