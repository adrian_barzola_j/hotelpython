from controller.TourdDetalleControlador import TourDetalleControlador

class TourDetalle:
    def __init__(self, 
            id_tour_detalle = None,
            id_tour = None,
            actividad = None):

            self._id_tour_detalle = id_tour_detalle
            self._id_tour = id_tour
            self._actividad = actividad

            self._controlador = TourDetalleControlador()
    
    def insertar(self):
        self._controlador.insertar(self)

    def actualizar(self):
        self._controlador.actualizar(self)

    def eliminar(self):
        self._controlador.eliminar(self)
    
    def consultar_uno(self, record_id):
        tupla_tour_detalle = self._controlador.consultar_uno(record_id)
        if tupla_tour_detalle:
            return TourDetalle(tupla_tour_detalle[0],tupla_tour_detalle[1],tupla_tour_detalle[2])

    def consultar_muchos(self, id_tour):
        lista_tour_detalle = []
        lista_tour_detalle = self._controlador._consultar_muchos(id_tour)
        for tupla_tour_detalle in lista_tour_detalle:
            lista_tour_detalle.append(TourDetalle(tupla_tour_detalle[0],tupla_tour_detalle[1],tupla_tour_detalle[2]))
