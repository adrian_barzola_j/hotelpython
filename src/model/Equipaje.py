from controller.EquipajeControlador import EquipajeControlador
class Equipaje:
    def __init__(self,
                id_equipaje = None,
                id_reserva = None,
                descripcion = None,
                tipo = None,
                valor_estimado = None,
                resumen = None,
                cuidados_especiales = None):
            self._id_equipaje = id_equipaje
            self._id_reserva = id_reserva
            self._descripcion = descripcion
            self._tipo = tipo
            self._valor_estimado = valor_estimado
            self._resumen = resumen
            self._cuidados_especiales = cuidados_especiales

            self._controlador = EquipajeControlador()
    
    def insertar(self):
        return self._controlador.insertar(self)

    def actualizar(self):
        return self._controlador.actualizar(self)

    def eliminar(self):
        return self._controlador.eliminar(self)
    
    def consultar_uno(self, id_equipaje):
        return self._controlador.consultar_uno(id_equipaje)

    def consultar_muchos(self, por_id_reserva = None, 
                               por_descripcion = None):
        return self._controlador._consultar_muchos(por_id_reserva, por_descripcion)
