class EncuestaTour:
    def __init__(self,
                id_encuesta_tour = None,
                id_reserva = None,
                id_cliente = None,
                id_detalle_tour = None,
                id_tour = None,
                valoracion = None,
                comentarios = None,
                fecha_encuesta = None):
        self._id_encuesta_tour = id_encuesta_tour
        self._id_reserva = id_reserva
        self._id_cliente = id_cliente
        self._id_detalle_tour = id_detalle_tour
        self._id_tour = id_tour
        self._valoracion = valoracion
        self._comentarios = comentarios
        self._fecha_encuesta = fecha_encuesta