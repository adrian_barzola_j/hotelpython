class FacturaDetalle:
    def __init__(self, 
                id_factura_detalle = None, 
                id_factura = None, 
                id_habitacion = None, 
                id_servicio_adicional = None,
                id_tour = None,
                cantidad = None, 
                precio = None
                ):
        self._id_factura_detalle = id_factura_detalle
        self._id_factura = id_factura
        self._id_habitacion = id_habitacion,
        self._id_servicio_adicional = id_servicio_adicional,
        self._id_tour = id_tour,
        self._cantidad = cantidad
        self._precio = precio