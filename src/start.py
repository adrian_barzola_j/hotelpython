
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from PIL import Image, ImageTk
from tkinter import messagebox
from view.ReservaDatosCliente import ReservaDatosCliente
from view.ConsultaEncuestaHospedajes import ConsultaEncuestasHospedajes
from view.ConsultaEncuestasActividades import ConsultaEncuestasActividades
from view.ConsultaEstadisticasReservas import ConsultaEstadisticasReservas
from view.ConsultaHabitaciones import ConsultaHabitaciones
from view.ConsultaHospedaje import ConsultaHospedaje
from view.CronogramaActividades import CronogramaActvidades
from view.EmisionFacturas import EmisionFacturas
from view.EncuestaActividades import EncuestaActividades
from view.EncuestaEstadia import EncuestaEstadia
from view.MantenimientoCheckOut import MantenimientoCheckOut
from view.MantenimientoCheckIn import MantenimientoCheckIn
from view.MantenimientoServiciosAdicionales import MantenimientoServiciosAdicionales
from view.MantenimientoTours import MantenimientoTours
from view.RegistroEquipajeEspecial import RegistroEquipajeEspecial
from view.RegistroMascotasPlantas import RegistroMascotasPlantas
from view.RegistroMedico import RegistroMedico
from view.ReservaAdicionales import ReservaAdicionales
from view.ReservaAgregaTour import ReservaAgregaTour
from view.ReservaDatosCliente import ReservaDatosCliente
from view.ReservaDatosHabitacion import ReservaDatosHabitacion
from view.ReservaPago import ReservaPago


# ventana = ThemedTk(themebg=True)
# aplicacion = EncuestaConsultaView(ventana)
# ventana.mainloop()
class Application:
    def __init__(self):

        root = ThemedTk(themebg=True)

        self._ventana = root

        root.set_theme(theme_name='ubuntu', themebg=True)

        root.title('Hotel Saturno')

        root.geometry("743x597")

        root.resizable(False, False)
        
        # Add image file
        bg = ImageTk.PhotoImage(file = 'D:/hotel_saturno/logo.png')

        canvas1 = Canvas( root, width = 743,
                        height = 597)
        
        canvas1.pack(fill = "both", expand = True)

        canvas1.create_image( 0, 0, image = bg, 
                            anchor = "nw")

        menubar = Menu(root)
        root.config(menu=menubar)

        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Salir", command=root.destroy)

        menu_estadisticas = Menu(menubar, tearoff=0)
        menu_estadisticas.add_command(label="Estadísticas de Reservas", command=self.abrir_estadisticas_reservas)
        menu_estadisticas.add_command(label="Consulta de Habitaciones", command=self.abrir_consulta_habitaciones)
        menu_estadisticas.add_command(label="Consulta de Huéspedes", command=self.abrir_consulta_huespedes)
        menu_estadisticas.add_command(label="Comentarios de Tours", command=self.abrir_consulta_encuesta_tour)
        menu_estadisticas.add_command(label="Comentarios de Hospedaje", command=self.abrir_cosulta_encuesta_hospedaje)

        menu_reservas = Menu(menubar, tearoff=0)
        menu_reservas.add_command(label="Ingreso Datos Cliente", command=self.abrir_registra_reserva)
        menu_reservas.add_command(label="Selección Hospedaje", command=self.abrir_advertencia_reserva)
        menu_reservas.add_command(label="Solicitar Servicios Adicionales", command=self.abrir_advertencia_reserva)
        menu_reservas.add_command(label="Agregar un Tour", command=self.abrir_advertencia_reserva)
        menu_reservas.add_command(label="Pagar la reserva", command=self.abrir_advertencia_reserva)
        menu_reservas.add_separator()
        menu_reservas.add_command(label="Ingreso / Check-In", command=self.abrir_checkin)
        menu_reservas.add_command(label="Registro Médico de Huéspedes", command=self.abrir_registro_medico)
        menu_reservas.add_command(label="Registro de Mascotas / Plantas", command=self.abrir_registro_mascotas)
        menu_reservas.add_command(label="Registro de Equipaje Especial", command=self.abrir_registro_equipaje)
        menu_reservas.add_separator()
        menu_reservas.add_command(label="Salida / Check-Out", command=self.abrir_checkout)
        menu_reservas.add_command(label="Imprimir Factura", command=self.abrir_factura)
        menu_reservas.add_separator()
        menu_reservas.add_command(label="Mantenimiento de Tours", command=self.abrir_mantenimiento_tour)
        menu_reservas.add_command(label="Mantenimiento de Servicios Adicionales", command=self.abrir_mantenimiento_adicionales)

        menu_clientes = Menu(menubar, tearoff=0)
        menu_clientes.add_command(label="Encuesta sobre estadía", command=self.abrir_encuesta_estadia)
        menu_clientes.add_command(label="Encuesta sobre los Tours", command=self.abrir_encuesta_tour)
        menu_clientes.add_command(label="Ver cronograma de Actividades", command=self.abrir_consulta_cronograma)

        menu_acerca = Menu(menubar, tearoff=0)
        menu_acerca.add_command(label="Acerca de...", command=self.mostrar_autor)

        menubar.add_cascade(label="Archivo", menu=filemenu)
        menubar.add_cascade(label="Estadísticas", menu=menu_estadisticas)
        menubar.add_cascade(label="Reservas", menu=menu_reservas)
        menubar.add_cascade(label="Clientes", menu=menu_clientes)
        menubar.add_cascade(label="Acerca de", menu=menu_acerca)

        # Finalmente bucle de la aplicación
        root.mainloop()
    
    def abrir_factura(self):
        messagebox.showwarning('Advertencia', 'Opción interna (Abrir Mantenimiento Ckeckout)')
    
    def abrir_registro_medico(self):
        ventana = Toplevel()
        aplicacion = RegistroMedico(self._ventana, ventana)    
        ventana.mainloop()
    
    def abrir_registra_reserva(self):
        ventana = Toplevel()
        aplicacion = ReservaDatosCliente(ventana)    
        ventana.mainloop()
    
    def abrir_estadisticas_reservas(self):
        ventana = Toplevel()
        aplicacion = ConsultaEstadisticasReservas(ventana)    
        ventana.mainloop()
    def abrir_consulta_habitaciones(self):
        ventana = Toplevel()
        aplicacion = ConsultaHabitaciones(ventana)    
        ventana.mainloop()
    def abrir_consulta_huespedes(self):
        ventana = Toplevel()
        aplicacion = ConsultaHospedaje(ventana)    
        ventana.mainloop()
    def abrir_consulta_encuesta_tour(self):
        ventana = Toplevel()
        aplicacion = ConsultaEncuestasActividades(ventana)    
        ventana.mainloop()
    def abrir_cosulta_encuesta_hospedaje(self):
        ventana = Toplevel()
        aplicacion = ConsultaEncuestasHospedajes(ventana)    
        ventana.mainloop()
    def abrir_advertencia_reserva(self):
        messagebox.showwarning('Advertencia', 'Opción interna (Abrir Registro de Reserva)')
    def abrir_checkin(self):
        ventana = Toplevel()
        aplicacion = MantenimientoCheckIn(self._ventana, ventana)    
        ventana.mainloop()
    def abrir_registro_mascotas(self):
        ventana = Toplevel()
        aplicacion = RegistroMascotasPlantas(self._ventana, ventana)    
        ventana.mainloop()
    def abrir_registro_equipaje(self):
        ventana = Toplevel()
        aplicacion = RegistroEquipajeEspecial(self._ventana, ventana)    
        ventana.mainloop()
    def abrir_checkout(self):
        ventana = Toplevel()
        aplicacion = MantenimientoCheckOut(self._ventana, ventana)    
        ventana.mainloop()
    def abrir_mantenimiento_tour(self):
        ventana = Toplevel() #ThemedTk(themebg=True)
        # ventana.set_theme(theme_name='ubuntu', themebg=True)
        aplicacion = MantenimientoTours(self._ventana, ventana)    
        ventana.mainloop()
    def abrir_mantenimiento_adicionales(self):
        ventana = Toplevel()
        aplicacion = MantenimientoServiciosAdicionales(ventana)    
        ventana.mainloop()
    def abrir_encuesta_estadia(self):
        ventana = Toplevel()
        aplicacion = EncuestaEstadia(ventana)    
        ventana.mainloop()
    def abrir_encuesta_tour(self):
        ventana = Toplevel()
        aplicacion = EncuestaActividades(ventana)    
        ventana.mainloop()
    def abrir_consulta_cronograma(self):
        ventana = Toplevel()
        aplicacion = CronogramaActvidades(ventana)    
        ventana.mainloop()
    def abrir_advertencia_checkout(self):
        messagebox.showwarning('Advertencia', 'Opción interna (Abrir Mantenimiento Checkout)')

    def mostrar_autor(self):
       messagebox.showinfo('Autor del Programa', '''
                Camila Ceballos
                Universidad de Guayaquil
                Hotel Saturno
                Derechos Reservados 2021
       ''')


if __name__ == '__main__':
    aplicacion = Application()