from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from tkcalendar import DateEntry

class ConsultaEstadisticasReservas:
    def __init__(self, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana.title('Estadística Reservas')
        self._ventana.geometry("800x450")

        marco = Frame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Estadísticas de Reservas', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = 'Check-In Inicio:', anchor='w').grid(row=2, column=1)
        Label(marco, text = 'Check-Out Inicio:', anchor='w').grid(row=4, column=1)
        Label(marco, text = 'Check-In Fin:', anchor='w').grid(row=2, column=3)
        Label(marco, text = 'Check-Out Fin:', anchor='w').grid(row=4, column=3)

        self._chkin_ini = DateEntry(marco, date_pattern='yyyy-MM-dd')
        self._chkin_ini.grid(row=2,column=2, padx = 12, pady = 3)

        self._chkin_fin = DateEntry(marco, date_pattern='yyyy-MM-dd')
        self._chkin_fin.grid(row=2,column=4, padx = 12, pady = 3)

        self._chkout_ini = DateEntry(marco, date_pattern='yyyy-MM-dd')
        self._chkout_ini.grid(row=4,column=2, padx = 12, pady = 3)

        self._chkout_fin = DateEntry(marco, date_pattern='yyyy-MM-dd')
        self._chkout_fin.grid(row=4,column=4, padx = 12, pady = 3)


        self._btn_buscar = ttk.Button(marco, text= 'Buscar'.center(25, ' '), command = self.buscar)
        self._btn_buscar.grid(row=6,column=1)
       
        self._tabla_consulta = ttk.Treeview(self._ventana)
        self._tabla_consulta['columns'] = ('Hab', 'NRes', 'NHos', 'Prom')
        self._tabla_consulta.grid(row=8, column = 1, padx = 40, pady = 20)
        self._tabla_consulta.column("#0", width=0, stretch=NO)
        self._tabla_consulta.column("#1", width=100)
        self._tabla_consulta.column("#2", width=100)
        self._tabla_consulta.column("#3", width=100)
        self._tabla_consulta.column("#4", width=100)
        
        # self._tabla_consulta.heading("#0", text='')
        self._tabla_consulta.heading("#1", text='Habitación')
        self._tabla_consulta.heading("#2", text='# Reservas')
        self._tabla_consulta.heading("#3", text='# Hospedajes')
        self._tabla_consulta.heading("#4", text='Prom. días')
        
    
    def buscar(self):
        pass


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = ConsultaEstadisticasReservas(ventana)    
    ventana.mainloop()