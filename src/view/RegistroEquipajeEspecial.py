from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from model.Equipaje import Equipaje

class RegistroEquipajeEspecial:
    def __init__(self, ventana_padre, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana_padre = ventana_padre
        
        self._ventana.title('Registro Equipaje Especial')
        self._ventana.geometry("600x400")

        marco = LabelFrame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Equipaje Especial', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = '# Reserva:').grid(row=2, column=1)
        Label(marco, text = 'Descripción:').grid(row=3, column=1)
        Label(marco, text = 'Tipo:').grid(row=4, column=1)
        Label(marco, text = 'Valor est.:').grid(row=5, column=1)
        Label(marco, text = 'Descripción física.:').grid(row=6, column=1)
        Label(marco, text = 'Cuidados especiales:').grid(row=8, column=1)

        self._no_reserva = Entry(marco)
        self._no_reserva.grid(row=2,column=2, padx = 12, pady = 3)

        self._nombre = Entry(marco)
        self._nombre.grid(row=3,column=2, padx = 12, pady = 3)

        self._tipo = ttk.Combobox(marco, values=['Arte','Armas','Joyas', 'Otros'])
        self._tipo.grid(row=4,column=2, padx = 12, pady = 3)
        self._tipo['state'] = 'readonly'

        self._valor = Entry(marco)
        self._valor.grid(row=5,column=2, padx = 12, pady = 3)
        
        self._descripcion = Entry(marco, width = 45)
        self._descripcion.grid(row=7,column=1,columnspan = 2, padx = 12, pady = 3)

        self._cuidados = Entry(marco, width = 45)
        self._cuidados.grid(row=9,column=1,columnspan = 2, padx = 12, pady = 3)

        self._btn_guardar = ttk.Button(self._ventana, text= 'Guardar'.center(25, ' '), command = self.guardar)
        self._btn_guardar.grid(row=10,column=1, columnspan=4, pady = 10)
    
    def guardar(self):
        equipaje = Equipaje(
                    id_equipaje = None,
                    id_reserva = self._no_reserva.get(),
                    descripcion = self._nombre.get(),
                    tipo = self._tipo.get(),
                    valor_estimado = self._valor.get(),
                    resumen = self._descripcion.get(),
                    cuidados_especiales = self._cuidados.get()
                )
        error = equipaje.insertar()
        if error:
            messagebox.showwarning('No se pudo guardar', error)
        else:
            messagebox.showinfo('Aviso', 'Guardado exitósamente')
            self._ventana_padre.deiconify()
            self._ventana.destroy()



if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = RegistroEquipajeEspecial(ventana)    
    ventana.mainloop()