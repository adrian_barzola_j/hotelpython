from os import sep
from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from tkcalendar import DateEntry
from model.Reserva import Reserva
from model.ReservaCliente import ReservaCliente
from model.ReservaDetalleAdicional import ReservaDetalleAdicional
from model.ReservaDetalleTour import ReservaDetalleTour
from model.ReservaDetalleHabitacion import ReservaDetalleHabitacion
from model.ServicioAdicional import ServicioAdicional
from model.Tour import Tour
from model.Habitacion import Habitacion
from model.Cliente import Cliente
from view.EmisionFacturas import EmisionFacturas
from datetime import datetime


class ReservaPago:
    def __init__(self, ventana:ThemedTk, datos_cliente):

        self._datos_cliente = datos_cliente
        self._ventana = ventana
        self._ventana.title('Pago Reservación')
        self._ventana.geometry("850x400")

        marco = LabelFrame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Confirma tu reserva', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = 'Nombre en la Tarjeta:').grid(row=2, column=2)
        Label(marco, text = 'Número de Tarjeta:   ').grid(row=3, column=2)
        Label(marco, text = 'CVV:                 ').grid(row=4, column=2)
        Label(marco, text = 'Fecha de Expiración: ').grid(row=5, column=2)
        Label(marco, text = 'A pagar hoy:         ').grid(row=6, column=2, rowspan = 2)


        self._nombretarjeta = Entry(marco)
        self._nombretarjeta.grid(row=2,column=3,columnspan = 2, padx = 12, pady = 3)

        self._numerotarjeta = Entry(marco)
        self._numerotarjeta.grid(row=3,column=3,columnspan = 2, padx = 12, pady = 3)

        self._tipotarjeta = ttk.Combobox(marco, values=['Visa', 'Mastercard', 'Amercian Ex'])
        self._tipotarjeta.grid(row=3,column=5,columnspan = 1, padx = 12, pady = 3)
        self._tipotarjeta['state'] = 'readonly'

        self._cvv = Entry(marco)
        self._cvv.grid(row=4,column=3,columnspan = 2, padx = 12, pady = 3)
        
        self._fecha_expiracion = DateEntry(marco, date_pattern='yyyy-MM-dd')
        self._fecha_expiracion.grid(row=5,column=3,columnspan = 2, padx = 12, pady = 3)

        self._total_pagar = Entry(marco, font=('Arial',22))
        self._total_pagar.grid(row=7,column=3, padx = 12, pady = 3, rowspan=2)
        self._total_pagar.insert(0,'1000')
        self._total_pagar['state'] = 'disabled'

        marco_botones = Frame(self._ventana)
        # , width=300, height=300)
        marco_botones.grid(row = 12, column = 1, columnspan = 4, pady = 20, padx = 150)

        self._btn_siguiente = ttk.Button(marco_botones, text= 'Siguiente'.center(25, ' '), command = self.guardar)
        self._btn_siguiente.grid(row=12,column=1, pady = 10)

        self._btn_cancelar = ttk.Button(marco_botones, text= 'Cancelar'.center(25, ' '), command = self.cancelar)
        self._btn_cancelar.grid(row=12,column=2, pady = 10)
    
    def guardar(self):

        info_cliente = self._datos_cliente[0]
        info_hab = tuple(self._datos_cliente[1])
        info_esp = tuple(self._datos_cliente[2])
        serv_adic = ServicioAdicional().consultar_nombre(info_esp[0])
        info_tour = self._datos_cliente[3]
        serv_tour = Tour().consultar_nombre(info_tour[0])

        r_reserva = Reserva(
                id_reserva = None,
                id_cliente = info_cliente[0][0],
                fecha_llegada = info_cliente[1],
                fecha_salida = info_cliente[2],
                no_adultos = info_cliente[3],
                no_ninos = info_cliente[4],
                nombre_tarjeta = self._nombretarjeta.get(),
                numero_tarjeta = self._numerotarjeta.get(),
                tipo_tarjeta = self._tipotarjeta.get(),
                fecha_expiracion = self._fecha_expiracion.get(),
                cvv = self._cvv.get(),
                total_pagar = self._total_pagar.get(),
                estado = 'Reservado'
        )

        # msg = r_reserva.insertar()
        # if msg is None:
        #     messagebox.showerror('Error', 'No se pudo guardar los datos')
        #     return
        # else:
        #     r_reserva._id_reserva = msg[0]

        # reserva_adic = ReservaDetalleAdicional(
        #     id_reserva_detalle_adicional = None,
        #     id_reserva = r_reserva._id_reserva,
        #     id_servicio_adicional = serv_adic._id_servicio_adicional,
        #     cantidad = 1
        # )

        # reserva_adic.insertar()
        
        messagebox.showinfo('Mensaje', 'Reserva realizada con éxito')
    
        data_reserva = []
        data_reserva.append(datetime.now().strftime("%Y-%m-%d"))
        cliente = Cliente().consultar_uno(r_reserva._id_cliente)
        data_reserva.append(cliente[1])
        data_reserva.append(cliente[2])
        data_reserva.append(cliente[4])
        data_reserva.append(cliente[3])
        data_reserva.append(cliente[6])
        data_reserva.append(int(r_reserva._no_ninos) + int(r_reserva._no_adultos))

        self.mostrar_factura(data_reserva)
        
        self._ventana.destroy()


    def cancelar(self):
        self._ventana.destroy()
    
    def mostrar_factura(self, data_factura):
        ventana = ThemedTk(themebg=True)
        ventana.set_theme(theme_name='ubuntu', themebg=True)
        aplicacion = EmisionFacturas(ventana, data_factura)    
        ventana.mainloop()


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = ReservaPago(ventana)    
    ventana.mainloop()