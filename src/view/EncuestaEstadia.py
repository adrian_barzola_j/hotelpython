from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from PIL import Image, ImageTk

class EncuestaEstadia:
    def __init__(self, ventana) -> None:
        self._ventana = ventana
        # self._ventana.resizable(False, False)
        self._ventana.geometry("700x650")

        self._ventana.resizable(False, False)

        marco = Frame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 3, columnspan = 4, pady = 20, padx = 150)

        title_1 = Label(marco, text = 'Cuéntanos sobre tu experiencia', font=('Arial',18))
        title_1.grid(row=2, column=2, columnspan=6)
        title_2 = Label(marco, text = 'durante tu estadía en Saturno', font=('Arial',18))
        title_2.grid(row=3, column=2, columnspan=6)

        self._btn_siguiente = ttk.Button(self._ventana, text= 'Siguiente'.center(25, ' '), command = self.guardar)
        self._btn_siguiente.grid(row=4,column=2, columnspan=6, pady = 10)

        marco_preguntas = Frame(self._ventana)
        # , width=300, height=300)
        marco_preguntas.grid(row = 6, column = 3, columnspan = 6, pady = 20, padx = 150)

        Label(marco_preguntas, text = '---------').grid(row=6, column=1)
        Label(marco_preguntas, text = 'Excelente').grid(row=6, column=2)
        Label(marco_preguntas, text = 'Muy bueno').grid(row=6, column=3)
        Label(marco_preguntas, text = 'Regular  ').grid(row=6, column=4)
        Label(marco_preguntas, text = 'Malo     ').grid(row=6, column=5)
        Label(marco_preguntas, text = 'Pésimo   ').grid(row=6, column=6)

        self._opcion_uno = IntVar()
        self._opcion_dos = IntVar()
        self._opcion_tres = IntVar()
        self._opcion_cuatro = IntVar()
        self._opcion_cinco = IntVar()

        Radiobutton(marco_preguntas, variable=self._opcion_uno, 
                value=5, command=self.opcion_encuesta).grid(row=7, column=2)
        Radiobutton(marco_preguntas, variable=self._opcion_uno,
                value=4, command=self.opcion_encuesta).grid(row=7, column=3)
        Radiobutton(marco_preguntas, variable=self._opcion_uno, 
                value=3, command=self.opcion_encuesta).grid(row=7, column=4)
        Radiobutton(marco_preguntas, variable=self._opcion_uno, 
                value=2, command=self.opcion_encuesta).grid(row=7, column=5)
        Radiobutton(marco_preguntas, variable=self._opcion_uno, 
                value=1, command=self.opcion_encuesta).grid(row=7, column=6)

        Label(marco_preguntas, text = 'Atención ').grid(row=7, column=1)
        Radiobutton(marco_preguntas, variable=self._opcion_dos, 
                value=5, command=self.opcion_encuesta).grid(row=7, column=2)
        Radiobutton(marco_preguntas, variable=self._opcion_dos,
                value=4, command=self.opcion_encuesta).grid(row=7, column=3)
        Radiobutton(marco_preguntas, variable=self._opcion_dos, 
                value=3, command=self.opcion_encuesta).grid(row=7, column=4)
        Radiobutton(marco_preguntas, variable=self._opcion_dos, 
                value=2, command=self.opcion_encuesta).grid(row=7, column=5)
        Radiobutton(marco_preguntas, variable=self._opcion_dos, 
                value=1, command=self.opcion_encuesta).grid(row=7, column=6)

        Label(marco_preguntas, text = 'Limpieza ').grid(row=8, column=1)
        Radiobutton(marco_preguntas, variable=self._opcion_tres, 
                value=5, command=self.opcion_encuesta).grid(row=8, column=2)
        Radiobutton(marco_preguntas, variable=self._opcion_tres,
                value=4, command=self.opcion_encuesta).grid(row=8, column=3)
        Radiobutton(marco_preguntas, variable=self._opcion_tres, 
                value=3, command=self.opcion_encuesta).grid(row=8, column=4)
        Radiobutton(marco_preguntas, variable=self._opcion_tres, 
                value=2, command=self.opcion_encuesta).grid(row=8, column=5)
        Radiobutton(marco_preguntas, variable=self._opcion_tres, 
                value=1, command=self.opcion_encuesta).grid(row=8, column=6)

        Label(marco_preguntas, text = 'Comida   ').grid(row=9, column=1)
        Radiobutton(marco_preguntas, variable=self._opcion_cuatro, 
                value=5, command=self.opcion_encuesta).grid(row=9, column=2)
        Radiobutton(marco_preguntas, variable=self._opcion_cuatro,
                value=4, command=self.opcion_encuesta).grid(row=9, column=3)
        Radiobutton(marco_preguntas, variable=self._opcion_cuatro, 
                value=3, command=self.opcion_encuesta).grid(row=9, column=4)
        Radiobutton(marco_preguntas, variable=self._opcion_cuatro, 
                value=2, command=self.opcion_encuesta).grid(row=9, column=5)
        Radiobutton(marco_preguntas, variable=self._opcion_cuatro, 
                value=1, command=self.opcion_encuesta).grid(row=9, column=6)

        Label(marco_preguntas, text = 'Ubicación').grid(row=10, column=1)
        Radiobutton(marco_preguntas, variable=self._opcion_cinco, 
                value=5, command=self.opcion_encuesta).grid(row=10, column=2)
        Radiobutton(marco_preguntas, variable=self._opcion_cinco,
                value=4, command=self.opcion_encuesta).grid(row=10, column=3)
        Radiobutton(marco_preguntas, variable=self._opcion_cinco, 
                value=3, command=self.opcion_encuesta).grid(row=10, column=4)
        Radiobutton(marco_preguntas, variable=self._opcion_cinco, 
                value=2, command=self.opcion_encuesta).grid(row=10, column=5)
        Radiobutton(marco_preguntas, variable=self._opcion_cinco, 
                value=1, command=self.opcion_encuesta).grid(row=10, column=6)
        
        Label(self._ventana, text = 'Comentarios').grid(row=12, column=4, rowspan=4)

        self._comentarios = Entry(self._ventana)
        self._comentarios.grid(row=15,column=4,columnspan = 6, rowspan=4, padx = 12, pady = 3)
    
    def guardar(self):
        pass

    def opcion_encuesta(self):
        pass


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = EncuestaEstadia(ventana)    
    ventana.mainloop()