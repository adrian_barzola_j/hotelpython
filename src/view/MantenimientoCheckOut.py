from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from tkcalendar import DateEntry

class MantenimientoCheckOut:
    def __init__(self, ventana_padre, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana_padre = ventana_padre
        self._ventana.title('Check Out')
        self._ventana.geometry("900x500")

        marco = Frame(self._ventana)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        
        Label(self._ventana, text = 'Check Out', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = 'No. Reserva:', anchor='w').grid(row=2, column=1)

        self._no_reserva = Entry(marco)
        self._no_reserva.grid(row=2,column=2, padx = 12, pady = 3)

        self._btn_buscar = ttk.Button(marco, text= 'Buscar'.center(25, ' '), command = self.buscar)
        self._btn_buscar.grid(row=6,column=1)

        self._btn_checkout = ttk.Button(marco, text= 'Facturar'.center(25, ' '), command = self.facturar)
        self._btn_checkout.grid(row=6,column=4)
       
        self._tabla_consulta = ttk.Treeview(self._ventana)
        self._tabla_consulta['columns'] = ('NRes', 'Id', 'Nom', 'Fin', 'FSal')
        self._tabla_consulta.grid(row=8, column = 1, padx = 40, pady = 20)
        self._tabla_consulta.column("#0", width=0, stretch=NO)
        self._tabla_consulta.column("#1", width=75)
        self._tabla_consulta.column("#2", width=100)
        self._tabla_consulta.column("#3", width=250)
        self._tabla_consulta.column("#4", width=100)
        self._tabla_consulta.column("#5", width=100)
        
        # self._tabla_consulta.heading("#0", text='')
        self._tabla_consulta.heading("#1", text='# Id')
        self._tabla_consulta.heading("#2", text='Cédula')
        self._tabla_consulta.heading("#3", text='Nombres')
        self._tabla_consulta.heading("#4", text='Fecha Ing.')
        self._tabla_consulta.heading("#5", text='Fecha Sal.')
        
    
    def facturar(self):
        self._ventana.destroy()
    
    def buscar(self):
        pass


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = MantenimientoCheckOut(ventana)    
    ventana.mainloop()