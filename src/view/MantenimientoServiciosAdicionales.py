from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from tkinter import filedialog
from PIL import ImageTk, Image
from model.ServicioAdicional import ServicioAdicional

class MantenimientoServiciosAdicionales:
    def __init__(self, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana.title('Registro de Adicionales')
        self._ventana.geometry("800x500")

        marco = LabelFrame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 6, column = 0, columnspan = 5, pady = 20, padx = 150)
        
        self._tmp_img_url = None
        Label(self._ventana, text = 'Registro de Adicionales', font=('Arial',22)).grid(row=0, column=1, padx=140)

        self._imagen_mostrar = Label(self._ventana, text = '<Imagen aquí>', width=30, height=10, anchor='c')
        self._imagen_mostrar.place(x=250, y=300, width=350, height=150)

        self._btn_guardar = ttk.Button(self._ventana, text= 'Guardar'.center(25, ' '), command = self.guardar)
        self._btn_guardar.grid(row=4,column=1, columnspan=4, pady = 10)

        Label(marco, text = 'Nombre:   ').grid(row=6, column=2)
        Label(marco, text = 'Costo:   ').grid(row=6, column=4)
        Label(marco, text = 'Membresía:      ').grid(row=7, column=2)
        Label(marco, text = 'Estado:     ').grid(row=7, column=4)
        Label(marco, text = 'Resumen:').grid(row=8, column=2)

        self._nombre = Entry(marco)
        self._nombre.grid(row=6,column=3, padx = 12, pady = 3)

        self._costo = Entry(marco)
        self._costo.grid(row=6,column=5, padx = 12, pady = 3)

        self._membresia = ttk.Combobox(marco, values=['Diaria', 'Semanal', 'Única'])
        self._membresia.grid(row=7,column=3, padx = 12, pady = 3)
        self._membresia['state'] = 'readonly'
        
        self._estado = ttk.Combobox(marco, values=['Activo', 'Inactivo'])
        self._estado.grid(row=7,column=5, padx = 12, pady = 3)
        self._estado['state'] = 'readonly'

        self._descripcion = Entry(marco, width=60)
        self._descripcion.grid(row=8,column=3, columnspan=3, padx = 12, pady = 3)

        self._btn_imagen = ttk.Button(self._ventana, text= '<Carga Imagen>'.center(25, ' '), command = self.evento_carga_imagen)
        self._btn_imagen.grid(row=12,column=1, columnspan=4, pady = 10)
    
    def guardar(self):
        servicio_adicional = ServicioAdicional(
            id_servicio_adicional = None,
            descripcion = self._nombre.get() ,
            costo = self._costo.get(),
            estado = self._estado.get(),
            membresia = self._membresia.get(),
            resumen = self._descripcion.get(),
            imagen_url = self._tmp_img_url
        )

        error = servicio_adicional.insertar()
        if error:
            messagebox.showwarning('No se pudo realizar la operación', error)
        else:
            messagebox.showinfo('Información','Registro Grabado Exitósamene')
            self._ventana.destroy()

    def evento_carga_imagen(self):
        archivo_imagen = filedialog.askopenfilename(filetypes=[('*.jpg','jpg'), ('*.png','png'), ('*.bmp','bmp'), ('*.jpeg','jpeg')])
        if archivo_imagen != '':
            self.carga_imagen(archivo_imagen)

    def carga_imagen(self, archivo_imagen=''):
        if archivo_imagen != '':
            self._tmp_img_url = archivo_imagen
            self._imagen = ImageTk.PhotoImage(Image.open(archivo_imagen))  
            self._imagen_mostrar['image'] = self._imagen


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = MantenimientoServiciosAdicionales(ventana)    
    ventana.mainloop()