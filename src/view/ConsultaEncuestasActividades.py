from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from tkcalendar import DateEntry

class ConsultaEncuestasActividades:
    def __init__(self, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana.title('Encuestas sobre los Tours')
        self._ventana.geometry("1000x450")

        marco = Frame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Estadísticas de Actividades', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = 'Fecha Inicio:', anchor='w').grid(row=2, column=1)
        Label(marco, text = 'Fecha Hasta:', anchor='w').grid(row=2, column=3)
        Label(marco, text = 'Actividad:', anchor='w').grid(row=3, column=1)
        Label(marco, text = 'Cliente:', anchor='w').grid(row=5, column=1)

        self._f_ini = DateEntry(marco, date_pattern='yyyy-MM-dd')
        self._f_ini.grid(row=2,column=2, padx = 12, pady = 3)

        self._f_fin = DateEntry(marco, date_pattern='yyyy-MM-dd')
        self._f_fin.grid(row=2,column=4, padx = 12, pady = 3)

        self._actividad = ttk.Combobox(marco)
        self._actividad.grid(row=3,column=2, padx = 12, pady = 3)
        self._actividad['state'] = 'readonly'

        self._cliente = ttk.Combobox(marco)
        self._cliente.grid(row=5,column=2, padx = 12, pady = 3)
        self._cliente['state'] = 'readonly'


        self._btn_buscar = ttk.Button(marco, text= 'Buscar'.center(25, ' '), command = self.buscar)
        self._btn_buscar.grid(row=6,column=1)
       
        self._tabla_consulta = ttk.Treeview(self._ventana)
        self._tabla_consulta['columns'] = ('Cli', 'Act', 'Val', 'Com')
        self._tabla_consulta.grid(row=8, column = 1, padx = 40, pady = 20)
        self._tabla_consulta.column("#0", width=0, stretch=NO)
        self._tabla_consulta.column("#1", width=250)
        self._tabla_consulta.column("#2", width=250)
        self._tabla_consulta.column("#3", width=75)
        self._tabla_consulta.column("#4", width=300)
        
        # self._tabla_consulta.heading("#0", text='')
        self._tabla_consulta.heading("#1", text='Cliente')
        self._tabla_consulta.heading("#2", text='Actividad')
        self._tabla_consulta.heading("#3", text='Valoración')
        self._tabla_consulta.heading("#4", text='Comentarios')
        
    
    def buscar(self):
        pass


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = ConsultaEncuestasActividades(ventana)    
    ventana.mainloop()