from model.ReservaDetalleMedico import ReservaDetalleMedico
from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk

class RegistroMedico:
    def __init__(self, ventana_padre, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana_padre = ventana_padre
        self._ventana.title('Registro Médico')
        self._ventana.geometry("900x400")

        marco = Frame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Ficha Médica', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = '# Reserva:', anchor='w').grid(row=1, column=1)
        Label(marco, text = 'Cliente:', anchor='w').grid(row=1, column=3)

        Label(marco, text = 'Peso::', anchor='w').grid(row=2, column=1)
        Label(marco, text = 'Raza:', anchor='w').grid(row=2, column=3)
        Label(marco, text = 'Tipo de Sangre:', anchor='w').grid(row=4, column=1)
        Label(marco, text = 'Sexo:', anchor='w').grid(row=4, column=3)
        Label(marco, text = 'Estatura:', anchor='w').grid(row=6, column=1)
        Label(marco, text = 'Enfermedades:', anchor='w').grid(row=7, column=1)        
        Label(marco, text = 'Alergias:', anchor='w').grid(row=9, column=1)
        Label(marco, text = 'Condiciones especiales:', anchor='w').grid(row=12, column=1)


        self._id_reserva = Entry(marco)
        self._id_reserva.grid(row=1,column=2, padx = 12, pady = 3)

        self._id_cliente = Entry(marco)
        self._id_cliente.grid(row=1,column=4, padx = 12, pady = 3)

        self._peso = Entry(marco)
        self._peso.grid(row=2,column=2, padx = 12, pady = 3)

        self._raza = ttk.Combobox(marco,values=[
                                        'Mestizo',
                                        'Blanco',
                                        'Indígena',
                                        'Afrodescendiente',
                                        'Montubio'
                                    ])
        self._raza.grid(row=2,column=4, padx = 12, pady = 3)
        self._raza['state'] = 'readonly'

        self._tipo_sangre =  ttk.Combobox(marco, values=['O+','O-', 'A+', 'A-', 'B+', 'B-', 'AB+', 'AB-'])
        self._tipo_sangre.grid(row=4,column=2, padx = 12, pady = 3)
        self._tipo_sangre['state'] = 'readonly'
        
        self._sexo = ttk.Combobox(marco, values=['Hombre', 'Mujer'])
        self._sexo.grid(row=4,column=4, padx = 12, pady = 3)
        self._sexo['state'] = 'readonly'

        self._estatura = Entry(marco)
        self._estatura.grid(row=6,column=2, padx = 12, pady = 3)

        self._enfermedades = Entry(marco, width=100)
        self._enfermedades.grid(row=8,column=1,columnspan = 5, padx = 12, pady = 3)
        
        self._alergias = Entry(marco, width=100)
        self._alergias.grid(row=11,column=1,columnspan = 5, padx = 12, pady = 3)

        self._condiciones_especiales = Entry(marco, width=100)
        self._condiciones_especiales.grid(row=13,column=1,columnspan = 5, padx = 12, pady = 3)

        self._btn_siguiente = ttk.Button(self._ventana, text= 'Guardar'.center(25, ' '), command = self.guardar)
        self._btn_siguiente.grid(row=15,column=1, columnspan=4, pady = 10)
    
    def guardar(self):
        
        detalle_medico = ReservaDetalleMedico(
                id_reserva_detalle_medico = None,
                id_reserva = self._id_reserva.get(),
                id_cliente = self._id_cliente.get(),
                peso = self._peso.get(),
                raza = self._raza.get(),
                tipo_sangre = self._tipo_sangre.get(),
                sexo = self._sexo.get(),
                estatura = self._estatura.get(),
                enfermedades = self._enfermedades.get(),
                alergias = self._alergias.get(),
                condiciones_especiales = self._condiciones_especiales.get()
        )

        msg = detalle_medico.insertar()
        if msg:
            messagebox.showwarning('No se pudo guardar', msg)
        else:
            messagebox.showinfo('Aviso', 'Guardado exitósamente')
            self._ventana_padre.deiconify()
            self._ventana.destroy()


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = RegistroMedico(ventana)    
    ventana.mainloop()