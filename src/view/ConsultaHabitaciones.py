from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from tkcalendar import DateEntry

class ConsultaHabitaciones:
    def __init__(self, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana.title('Consulta de Habitaciones')
        self._ventana.geometry("900x500")

        marco = Frame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Consulta de Habitaciones', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = 'Tipo:', anchor='w').grid(row=2, column=1)
        Label(marco, text = 'Estado:', anchor='w').grid(row=2, column=3)
        Label(marco, text = 'Check-In Inicio:', anchor='w').grid(row=4, column=1)
        Label(marco, text = 'Check-In Fin:', anchor='w').grid(row=4, column=3)
        Label(marco, text = 'Check-Out Inicio:', anchor='w').grid(row=6, column=1)
        Label(marco, text = 'Check-Out Fin:', anchor='w').grid(row=6, column=3)

        self._tipo = ttk.Combobox(marco,values=[
                                        'Sencilla',
                                        'Doble',
                                        'Cuádruple',
                                        'Compartida',
                                        'Presidencial'
                                    ])
        self._tipo.grid(row=2,column=2, padx = 12, pady = 3)
        self._tipo['state'] = 'readonly'

        self._estado =  ttk.Combobox(marco, values=['Libre', 'Ocupada', 'Reservada', 'Dañada', 'Mantenimiento'])
        self._estado.grid(row=2,column=4, padx = 12, pady = 3)
        self._estado['state'] = 'readonly'

        self._fecha_chkin_desde = DateEntry(marco, pattern = 'yyyy-MM-dd')
        self._fecha_chkin_desde.grid(row=4,column=2, padx = 12, pady = 3)

        self._fecha_chkin_hasta = DateEntry(marco, pattern = 'yyyy-MM-dd')
        self._fecha_chkin_hasta.grid(row=4,column=4, padx = 12, pady = 3)

        self._fecha_chkout_desde = DateEntry(marco, pattern = 'yyyy-MM-dd')
        self._fecha_chkout_desde.grid(row=6,column=2, padx = 12, pady = 3)

        self._fecha_chkout_hasta = DateEntry(marco, pattern = 'yyyy-MM-dd')
        self._fecha_chkout_hasta.grid(row=6,column=4, padx = 12, pady = 3)

        self._btn_buscar = ttk.Button(marco, text= 'Buscar'.center(25, ' '), command = self.buscar)
        self._btn_buscar.grid(row=8,column=1)
       
        self._tabla_consulta = ttk.Treeview(self._ventana)
        self._tabla_consulta['columns'] = ('ID', 'Hab', 'Tip', 'ChkIn', 'ChkOu', 'Est')
        self._tabla_consulta.grid(row=10, column = 1, padx = 40, pady = 20)
        self._tabla_consulta.column("#0", width=0, stretch=NO)
        self._tabla_consulta.column("#1", width=100)
        self._tabla_consulta.column("#2", width=150)
        self._tabla_consulta.column("#3", width=150)
        self._tabla_consulta.column("#4", width=100)
        self._tabla_consulta.column("#5", width=100)
        self._tabla_consulta.heading("#0", text='')
        self._tabla_consulta.heading("#1", text='Hab')
        self._tabla_consulta.heading("#2", text='Tipo')
        self._tabla_consulta.heading("#3", text='Checked-In')
        self._tabla_consulta.heading("#4", text='Checked-Out')
        self._tabla_consulta.heading("#5", text='Estado')
    
    def buscar(self):
        pass


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = ConsultaHabitaciones(ventana)    
    ventana.mainloop()