from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from PIL import Image, ImageTk
from view.ReservaAgregaTour import ReservaAgregaTour
from model.ServicioAdicional import ServicioAdicional

class ReservaAdicionales:
    def __init__(self, ventana, datos_cliente):
        self._datos_cliente = datos_cliente
        self._ventana = ventana
        # self._ventana.resizable(False, False)
        self._ventana.geometry("900x650")

        self._ventana.resizable(False, False)

        title_1 = Label(self._ventana, text = 'Servicios Adicionales', font=('Arial',18))
        title_1.pack(padx=140)
        title_2 = Label(self._ventana, text = 'para una Estadía perfecta', font=('Arial',18))
        title_2.pack(padx=140)

        self._btn_siguiente = ttk.Button(self._ventana, text= 'Siguiente'.center(25, ' '), command = self.guardar)
        self._btn_siguiente.pack()

        s = ttk.Style()
        s.configure('Treeview', rowheight=150)

        tree_frame = Frame(self._ventana)
        tree_frame.pack(pady=30) 
        # grid(row=2, column=0, sticky='nsew', padx=50, columnspan=3)

        tree_scroll = Scrollbar(tree_frame)
        tree_scroll.pack(side=RIGHT, fill=Y)

        # Create Treeview 
        self.tree = ttk.Treeview(tree_frame, column=('A','B'), yscrollcommand=tree_scroll.set)
        self.tree.pack()
        # grid(row=0, column=0)

        tree_scroll.config(command=self.tree.yview)

        # Setup column heading
        self.tree.heading('#0', text='', anchor='center')
        self.tree.heading('#1', text='', anchor='center')
        self.tree.heading('#2', text='', anchor='center')
        
        # #0, #01, #02 denotes the 0, 1st, 2nd columns

        # Setup column
        self.tree.column('#0', anchor='center', width=400)
        self.tree.column('#1', anchor='center', width=100)
        self.tree.column('#2', anchor='center', width=300)

        self._imgs = []
        self._pimgs = []

        self.busca_adicionales()

        

    def guardar(self):
        id_adicional = str( self.tree.item(self.tree.selection())['values'][0] )
        self._ventana.destroy()
        ventana = Toplevel()
        self._datos_cliente.append((id_adicional,))
        aplicacion = ReservaAgregaTour(ventana, self._datos_cliente)    
        ventana.mainloop()
    
    def busca_adicionales(self):
        for rec in self.tree.get_children():
            self.tree.delete(rec)
        serv_adic = ServicioAdicional()
        lista_servicios = serv_adic.consultar_muchos()
        for servicio in lista_servicios:
            image = Image.open(servicio[5])
            self._imgs.append(image)
            pimg = ImageTk.PhotoImage(image) #change to your file path
            self._pimgs.append(pimg)
            self.tree.insert('', 'end', image=pimg,
                         value=(servicio[1], servicio[4])) 

if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = ReservaAdicionales(ventana)    
    ventana.mainloop()                       