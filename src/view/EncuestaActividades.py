from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from PIL import Image, ImageTk

class EncuestaActividades:
    def __init__(self, ventana):

        self._ventana = ventana
        # self._ventana.resizable(False, False)
        self._ventana.geometry("600x550")

        self._ventana.resizable(False, False)

        title_1 = Label(self._ventana, text = 'Encuesta de Actividades', font=('Arial',18))
        title_1.pack(padx=140)
        # title_2 = Label(self._ventana, text = 'para una Estadía perfecta', font=('Arial',18))
        # title_2.pack(padx=140)

        self._btn_siguiente = ttk.Button(self._ventana, text= 'Siguiente'.center(25, ' '), command = self.guardar)
        self._btn_siguiente.pack()

        # s = ttk.Style()
        # s.configure('Treeview', rowheight=150)

        tree_frame = Frame(self._ventana)
        tree_frame.pack(pady=20) 
        # grid(row=2, column=0, sticky='nsew', padx=50, columnspan=3)

        tree_scroll = Scrollbar(tree_frame)
        tree_scroll.pack(side=RIGHT, fill=Y)

        # Create Treeview 
        self.tree = ttk.Treeview(tree_frame, column=('A','B'), yscrollcommand=tree_scroll.set)
        self.tree.pack()
        # grid(row=0, column=0)

        tree_scroll.config(command=self.tree.yview)

        # Setup column heading
        self.tree.heading('#0', text='', anchor='center')
        self.tree.heading('#1', text='Actividad', anchor='center')
        self.tree.heading('#2', text='Fecha', anchor='center')
        
        # #0, #01, #02 denotes the 0, 1st, 2nd columns

        # Setup column
        self.tree.column('#0', anchor='center', width=0)
        self.tree.column('#1', anchor='w', width=250)
        self.tree.column('#2', anchor='w', width=100)


        self.tree.insert('', 'end',
                         value=("A's value", "B0's value"))

        
        Label(self._ventana, text = 'Tu valoración es:').pack(padx=140)
        self.valoracion = ttk.Combobox(self._ventana, values = [1,2,3,4,5,6,7,8,9,10], font=('Arial',18))
        self.valoracion.pack(padx=140)

        Label(self._ventana, text = 'Comentarios:').pack(padx=140)
        self.comentario_valoracion = Text(self._ventana,padx=110, pady=110)
        self.comentario_valoracion.pack()

        
        # .grid(row=12,column=1, columnspan=4, pady = 10)

    def guardar(self):
        pass                                              

if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = EncuestaActividades(ventana)    
    ventana.mainloop()                       