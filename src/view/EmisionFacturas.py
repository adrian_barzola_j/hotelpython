from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from tkcalendar import DateEntry

class EmisionFacturas:
    def __init__(self, ventana:ThemedTk, data_reserva):
        self._ventana = ventana
        self._ventana.title('Factura')
        self._ventana.geometry("1000x600")
        self._data_reserva = data_reserva

        marco = Frame(self._ventana)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        
        Label(self._ventana, text = 'Factura', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = 'Fecha de Emsión:', anchor='w').grid(row=2, column=0)
        Label(marco, text = 'Identificación:', anchor='w').grid(row=3, column=0)
        Label(marco, text = 'Nombre:', anchor='w').grid(row=4, column=0)
        Label(marco, text = 'Dirección:', anchor='w').grid(row=5, column=0)
        Label(marco, text = 'Teléfono:', anchor='w').grid(row=6, column=0)        
        Label(marco, text = 'Email:', anchor='w').grid(row=7, column=0)
        Label(marco, text = 'Nº 11005:', anchor='w', font=('Arial',16)).grid(row=2, column=2)

        Label(marco, anchor = 'n', text = '''
            RUC: 1326598745001
            RAZON SOCIAL: HOTEL SATURNO S.A.
            MATRIZ: 25 Y PORTETE
            AUT. SRI: 231032103213213216546
        ''').grid(row=3, column=2, rowspan=5)


        self._f_emision = Entry(marco)
        self._f_emision.grid(row=2,column=1, padx = 12, pady = 3)
        self._f_emision.insert(0,self._data_reserva[0])
        self._f_emision['state'] = 'disabled'

        self._identificacion = Entry(marco)
        self._identificacion.grid(row=3,column=1, padx = 12, pady = 3)
        self._identificacion.insert(0,self._data_reserva[1])
        self._identificacion['state'] = 'disabled'

        self._nombre = Entry(marco)
        self._nombre.grid(row=4,column=1, padx = 12, pady = 3)
        self._nombre.insert(0,self._data_reserva[2])
        self._nombre['state'] = 'disabled'

        self._direccion = Entry(marco)
        self._direccion.grid(row=5,column=1, padx = 12, pady = 3)
        self._direccion.insert(0,self._data_reserva[3])
        self._direccion['state'] = 'disabled'

        self._telefono = Entry(marco)
        self._telefono.grid(row=6,column=1, padx = 12, pady = 3)
        self._telefono.insert(0,self._data_reserva[4])
        self._telefono['state'] = 'disabled'

        self._email = Entry(marco)
        self._email.grid(row=7,column=1, padx = 12, pady = 3)
        self._email.insert(0,self._data_reserva[5])
        self._email['state'] = 'disabled'
       
        self._tabla_consulta = ttk.Treeview(marco)
        self._tabla_consulta['columns'] = ('Item', 'Can', 'Pr', 'St')
        self._tabla_consulta.grid(row=8, column = 0, columnspan=5, padx = 40, pady = 20)
        self._tabla_consulta.column("#0", width=0, stretch=NO)
        self._tabla_consulta.column("#1", width=300)
        self._tabla_consulta.column("#2", width=150)
        self._tabla_consulta.column("#3", width=150)
        self._tabla_consulta.column("#4", width=150)
        
        # self._tabla_consulta.heading("#0", text='')
        self._tabla_consulta.heading("#1", text='Item')
        self._tabla_consulta.heading("#2", text='Cantidad')
        self._tabla_consulta.heading("#3", text='Precio')
        self._tabla_consulta.heading("#4", text='Subtotal')

        no_personas = int(self._data_reserva[6])
        subtotal_12 = ( no_personas * 90 ) + (no_personas * 45 ) + (no_personas * 200 )
        iva = round(subtotal_12 * 0.12,2)

        self._tabla_consulta.insert('', 'end',
                         value=('Hospedaje', no_personas, '90', no_personas * 90 ))
        self._tabla_consulta.insert('', 'end',
                         value=('Adicionales',no_personas, '45', no_personas * 45 ))
        self._tabla_consulta.insert('', 'end',
                         value=('Tour', no_personas, '200', no_personas * 200 ))

        Label(marco, text = 'Subtotal  0%:  ', anchor='w').grid(row=22, column=2)
        Label(marco, text = 'Subtotal 12%:  ', anchor='w').grid(row=23, column=2)
        Label(marco, text = 'IVA:           ', anchor='w').grid(row=24, column=2)
        Label(marco, text = 'Total a pagar :', anchor='w').grid(row=25, column=2)
        self._subtotal_0 = Entry(marco)
        self._subtotal_0.grid(row=22,column=3, padx = 12, pady = 3)
        self._subtotal_0.insert(0,0)
        self._subtotal_0['state'] = 'disabled'
        self._subtotal_12 = Entry(marco)
        self._subtotal_12.grid(row=23,column=3, padx = 12, pady = 3)
        self._subtotal_12.insert(0,subtotal_12)
        self._subtotal_12['state'] = 'disabled'
        self._iva = Entry(marco)
        self._iva.grid(row=24,column=3, padx = 12, pady = 3)
        self._iva.insert(0, iva)
        self._iva['state'] = 'disabled'
        self._total_pagar = Entry(marco)
        self._total_pagar.grid(row=25,column=3, padx = 12, pady = 3)
        self._total_pagar.insert(0, f'{subtotal_12 + iva}')
        self._total_pagar['state'] = 'disabled'
        
    
    def agregar_registro(self):
        pass
    
    def guardar(self):
        pass

    def quitar_registro(self):
        pass


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = EmisionFacturas(ventana)    
    ventana.mainloop()