from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from tkcalendar import DateEntry
from model.Cliente import Cliente
from model.ReservaCliente import ReservaCliente
from datetime import datetime

class MantenimientoCheckIn:
    def __init__(self, ventana_padre, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana_padre = ventana_padre
        self._ventana.title('Check In')
        self._ventana.geometry("900x500")

        marco = Frame(self._ventana)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        
        Label(self._ventana, text = 'Check In', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = 'No. Reserva:', anchor='w').grid(row=2, column=1)
        Label(marco, text = 'Identificación:', anchor='w').grid(row=2, column=3)
        Label(marco, text = 'Nombre:', anchor='w').grid(row=3, column=1)
        Label(marco, text = 'Dirección:', anchor='w').grid(row=3, column=3)
        Label(marco, text = 'Teléfono:', anchor='w').grid(row=4, column=1)
        Label(marco, text = 'Fecha de Nacimiento:', anchor='w').grid(row=4, column=3)
        Label(marco, text = 'Email:', anchor='w').grid(row=5, column=1)
        Label(marco, text = 'Equipaje:', anchor='w').grid(row=5, column=3)

        self._no_reserva = Entry(marco)
        self._no_reserva.grid(row=2,column=2, padx = 12, pady = 3)

        self._identificacion = Entry(marco)
        self._identificacion.grid(row=2,column=4, padx = 12, pady = 3)

        self._nombre = Entry(marco)
        self._nombre.grid(row=3,column=2, padx = 12, pady = 3)

        self._direccion = Entry(marco)
        self._direccion.grid(row=3,column=4, padx = 12, pady = 3)

        self._telefono = Entry(marco)
        self._telefono.grid(row=4,column=2, padx = 12, pady = 3)

        self._f_nac = DateEntry(marco, date_pattern='yyyy-MM-dd')
        self._f_nac.grid(row=4,column=4, padx = 12, pady = 3)

        self._email = Entry(marco)
        self._email.grid(row=5,column=2, padx = 12, pady = 3)

        self._equipaje = ttk.Combobox(marco, values=[1,2,3,4,5])
        self._equipaje.grid(row=5,column=4, padx = 12, pady = 3)


        self._btn_agregar = ttk.Button(marco, text= 'Agregar'.center(25, ' '), command = self.agregar_registro)
        self._btn_agregar.grid(row=6,column=1)
        # self._btn_quitar = ttk.Button(marco, text= 'Quitar'.center(25, ' '), command = self.quitar_registro)
        # self._btn_quitar.grid(row=6,column=2)

        # self._btn_guardar = ttk.Button(marco, text= 'Guardar'.center(25, ' '), command = self.guardar)
        # self._btn_guardar.grid(row=6,column=4)
       
        self._tabla_consulta = ttk.Treeview(self._ventana)
        self._tabla_consulta['columns'] = ('Id', 'Ced', 'Nom', 'Fin', 'FSal')
        self._tabla_consulta.grid(row=8, column = 1, padx = 40, pady = 20)
        self._tabla_consulta.column("#0", width=0, stretch=NO)
        self._tabla_consulta.column("#1", width=75)
        self._tabla_consulta.column("#2", width=100)
        self._tabla_consulta.column("#3", width=250)
        self._tabla_consulta.column("#4", width=100)
        self._tabla_consulta.column("#5", width=100)
        
        # self._tabla_consulta.heading("#0", text='')
        self._tabla_consulta.heading("#1", text='# ID')
        self._tabla_consulta.heading("#2", text='Cédula')
        self._tabla_consulta.heading("#3", text='Nombres')
        self._tabla_consulta.heading("#4", text='Fecha Ing.')
        self._tabla_consulta.heading("#5", text='Fecha Sal.')
        
    
    def agregar_registro(self):
        cliente_modelo = Cliente(id_cliente = None, 
                        identificacion = self._identificacion.get(),
                        nombre = self._nombre.get(),
                        telefono = self._telefono.get(),
                        direccion = self._direccion.get(),
                        fecha_nacimiento = self._f_nac.get()
                )
        cliente = cliente_modelo.consulta_identificacion(cliente_modelo._identificacion)
        if cliente is None:
            msg = cliente_modelo.insertar()
            if msg:
                messagebox.showwarning('No se pudo guardar', msg)
                return None
            cliente = cliente_modelo.consulta_identificacion(cliente_modelo._identificacion)

        reserva_cliente = ReservaCliente(
                id_reserva_cliente = None,
                id_reserva = self._no_reserva.get(),
                id_cliente = cliente[0],
                no_maletas = self._equipaje.get(),
                fecha_ingreso = datetime.today().strftime('%Y-%m-%d'),
                fecha_salida = None,
                observacion = None
        )

        msg = reserva_cliente.insertar()
        if msg:
            messagebox.showwarning('No se pudo guardar', msg)
        else:
            messagebox.showinfo('Aviso', 'Guardado exitósamente')
            self.limpiar_cliente()
            self.llena_tabla()
    
    def llena_tabla(self):
        for rec in self._tabla_consulta.get_children():
            self._tabla_consulta.delete(rec)
        controller = ReservaCliente()
        for record in controller.consultar_muchos(id_reserva = self._no_reserva.get()):
            cliente = Cliente(id_cliente = record[2])
            cliente = cliente.consultar_uno(cliente._id_cliente)
            self._tabla_consulta.insert('', 'end', 
                         value=(record[0], cliente[1], cliente[2], record[4], ''))
    
    def limpiar_cliente(self):
        self._identificacion.delete(0,END)
        self._nombre.delete(0,END)
        self._telefono.delete(0,END)
        self._direccion.delete(0,END)
        self._f_nac.delete(0,END)
        self._email.delete(0,END)

    
    def guardar(self):
        pass

    def quitar_registro(self):
        pass


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = MantenimientoCheckIn(ventana)    
    ventana.mainloop()