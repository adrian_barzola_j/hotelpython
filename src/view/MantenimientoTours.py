from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from ttkthemes import ThemedTk
from PIL import ImageTk, Image
from model.Tour import Tour
from model.TourDetalle import TourDetalle

class MantenimientoTours:
    def __init__(self, ventana_padre:ThemedTk, ventana:ThemedTk):
        self._ventana_padre = ventana_padre
        # self._ventana_padre.withdraw()
        self._ventana = ventana
        self._ventana.title('Registro de Tours')
        self._ventana.geometry("800x500")

        marco = LabelFrame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 6, column = 0, columnspan = 5, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Registro de Tours', font=('Arial',22)).grid(row=0, column=1, padx=140)

        self._imagen_mostrar = Label(self._ventana, text = '<Imagen aquí>', width=30, height=10, anchor='c')
        self._imagen_mostrar.place(x=250, y=300, width=350, height=150)
        # .grid(row=2,column=1,columnspan = 2, rowspan=2, padx = 12, pady = 3)

        self._btn_imagen = ttk.Button(self._ventana, text= 'Guardar'.center(25, ' '), command = self.guardar)
        self._btn_imagen.grid(row=4,column=1, columnspan=4, pady = 10)

        Label(marco, text = 'Nombre:     ').grid(row=6, column=2)
        Label(marco, text = 'Duración:   ').grid(row=6, column=4)
        Label(marco, text = 'Costo:      ').grid(row=7, column=2)
        Label(marco, text = 'Estado:     ').grid(row=7, column=4)
        Label(marco, text = 'Descripción:').grid(row=8, column=2)

        self._nombre = Entry(marco)
        self._nombre.grid(row=6,column=3, padx = 12, pady = 3)

        self._duracion = Entry(marco)
        self._duracion.grid(row=6,column=5, padx = 12, pady = 3)

        self._costo = Entry(marco)
        self._costo.grid(row=7,column=3, padx = 12, pady = 3)
        
        self._estado = ttk.Combobox(marco, values=['Activo', 'Inactivo'])
        self._estado.grid(row=7,column=5, padx = 12, pady = 3)
        self._estado['state'] = 'readonly'

        self._descripcion = Entry(marco, width=60)
        self._descripcion.grid(row=8,column=3, columnspan=3, padx = 12, pady = 3)

        self._btn_guardar = ttk.Button(self._ventana, text= 'Agregar Imagen'.center(25, ' '), command = self.evento_carga_imagen)
        self._btn_guardar.grid(row=12,column=1, columnspan=4, pady = 10)

        self._imagen = None
        self._tmp_img_url = None
    
    def guardar(self):
        tour_record = Tour(
                 id_tour = None
                ,nombre = self._nombre.get()
                ,duracion = self._duracion.get()
                ,costo = self._costo.get()
                ,estado = self._estado.get()
                ,resumen = self._descripcion.get()
                ,imagen_url = self._tmp_img_url
            )
        msg = tour_record.insertar()
        if msg:
            messagebox.showwarning('No se pudo guardar', msg)
        else:
            messagebox.showinfo('Aviso', 'Guardado exitósamente')
            # self._ventana_padre.deiconify()
            self._ventana.destroy()

    def evento_carga_imagen(self):
        archivo_imagen = filedialog.askopenfilename(filetypes=[('*.jpg','jpg'), ('*.png','png'), ('*.bmp','bmp'), ('*.jpeg','jpeg')])
        if archivo_imagen != '':
            self.carga_imagen(archivo_imagen)

    def carga_imagen(self, archivo_imagen=''):
        if archivo_imagen != '':
            self._tmp_img_url = archivo_imagen
            self._imagen = ImageTk.PhotoImage(Image.open(archivo_imagen))  
            self._imagen_mostrar['image'] = self._imagen


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = MantenimientoTours(ventana)    
    ventana.mainloop()