from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from model.Mascota import Mascota

class RegistroMascotasPlantas:
    def __init__(self, ventana_padre, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana_padre = ventana_padre
        self._ventana.title('Acompañantes no Humanos')
        self._ventana.geometry("600x400")

        marco = LabelFrame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Registro de Mascotas', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = '# Reserva:').grid(row=2, column=1)
        Label(marco, text = 'Nombre:').grid(row=3, column=1)
        Label(marco, text = 'Tipo:').grid(row=4, column=1)
        Label(marco, text = 'Descripción física:').grid(row=5, column=1)
        Label(marco, text = 'Cuidados especiales:').grid(row=7, column=1)

        self._no_reserva = Entry(marco)
        self._no_reserva.grid(row=2,column=2,columnspan = 5, padx = 12, pady = 3)

        self._nombre = Entry(marco)
        self._nombre.grid(row=3,column=2,columnspan = 5, padx = 12, pady = 3)

        self._tipo = ttk.Combobox(marco, values=['Perro','Gato','Ave','Planta', 'Otros'])
        self._tipo.grid(row=4,column=2,columnspan = 2, padx = 12, pady = 3)
        self._tipo['state'] = 'readonly'
        
        self._descripcion = Entry(marco, width = 45)
        self._descripcion.grid(row=6,column=1,columnspan = 2, padx = 12, pady = 3)

        self._cuidados = Entry(marco, width = 45)
        self._cuidados.grid(row=8,column=1,columnspan = 2, padx = 12, pady = 3)

        self._btn_guardar = ttk.Button(self._ventana, text= 'Guardar'.center(25, ' '), command = self.guardar)
        self._btn_guardar.grid(row=12,column=1, columnspan=4, pady = 10)
    
    def guardar(self):
        mascota = Mascota(
                id_mascota =None,
                id_reserva =self._no_reserva.get(),
                nombre = self._nombre.get(),
                tipo = self._tipo.get(),
                descripcion =self._descripcion.get(),
                cuidados_especiales = self._descripcion.get()
        )

        msg = mascota.insertar()
        if msg:
            messagebox.showwarning('No se pudo guardar', msg)
        else:
            messagebox.showinfo('Aviso', 'Guardado exitósamente')
            self._ventana_padre.deiconify()
            self._ventana.destroy()


if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = RegistroMascotasPlantas(ventana)    
    ventana.mainloop()