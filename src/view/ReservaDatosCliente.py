from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk
from view.ReservaDatosHabitacion import ReservaDatosHabitacion
from model.Cliente import Cliente
from tkcalendar import DateEntry

class ReservaDatosCliente:
    def __init__(self, ventana:ThemedTk):
        self._ventana = ventana
        self._ventana.title('Reservación')
        self._ventana.geometry("600x400")

        marco = LabelFrame(self._ventana)
        # , width=300, height=300)
        marco.grid(row = 2, column = 0, columnspan = 4, pady = 20, padx = 150)
        

        Label(self._ventana, text = 'Haz una reserva ahora', font=('Arial',22)).grid(row=0, column=1, padx=140)

        Label(marco, text = 'Identificación:      ').grid(row=2, column=2)
        Label(marco, text = 'Nombre completo:     ').grid(row=3, column=2)
        Label(marco, text = 'Correo electrónico:  ').grid(row=4, column=2)
        Label(marco, text = 'Teléfono:            ').grid(row=5, column=2)
        Label(marco, text = 'Fecha de Llegada:    ').grid(row=6, column=2)
        Label(marco, text = 'Fecha de Salida:     ').grid(row=7, column=2)        
        Label(marco, text = 'Adultos:             ').grid(row=8, column=2)
        Label(marco, text = 'Niños:               ').grid(row=9, column=2)

        self.cedula = StringVar()
        self._identificacion = Entry(marco, 
                                        textvariable=self.cedula, 
                                        validate='focusout', 
                                        validatecommand=self.busca_cliente)
        
        self._identificacion.grid(row=2,column=3,columnspan = 2, padx = 12, pady = 3)

        self._nombre = Entry(marco)
        self._nombre.grid(row=3,column=3,columnspan = 5, padx = 12, pady = 3)

        self._email = Entry(marco)
        self._email.grid(row=4,column=3,columnspan = 2, padx = 12, pady = 3)
        
        self._telefono = Entry(marco)
        self._telefono.grid(row=5,column=3,columnspan = 2, padx = 12, pady = 3)

        self._fecha_salida = DateEntry(marco, date_pattern = 'yyyy-MM-dd')
        self._fecha_salida.grid(row=6,column=3,columnspan = 2, padx = 12, pady = 3)

        self._fecha_entrada = DateEntry(marco, date_pattern = 'yyyy-MM-dd')
        self._fecha_entrada.grid(row=7,column=3,columnspan = 2, padx = 12, pady = 3)
        
        self._adultos = ttk.Combobox(marco, values=[1,2,3,4,5,6,7,8,9,10])
        self._adultos.grid(row=8,column=3,columnspan = 2, padx = 12, pady = 3)
        self._adultos['state'] = 'readonly'

        self._ninos = ttk.Combobox(marco, values=[1,2,3,4,5,6,7,8,9,10])
        self._ninos.grid(row=9,column=3,columnspan = 2, padx = 12, pady = 3)
        self._ninos['state'] = 'readonly'

        self._btn_siguiente = ttk.Button(self._ventana, text= 'Siguiente'.center(25, ' '), command = self.guardar)
        self._btn_siguiente.grid(row=12,column=1, columnspan=4, pady = 10)

        self._cliente = None
    
    def guardar(self):
        ventana = Toplevel()
        datos_cliente = (self._cliente, self._fecha_entrada.get() ,self._fecha_salida.get(), self._adultos.get(), self._ninos.get())
        aplicacion = ReservaDatosHabitacion(ventana, [datos_cliente])
        self._ventana.destroy()
        ventana.mainloop()
    
    def busca_cliente(self):
        serv_cliente = Cliente().consulta_identificacion(self._identificacion.get())
        if serv_cliente:
            self._cliente = serv_cliente
            self._nombre.delete(0,END)
            self._nombre.insert(0,serv_cliente[2])
            self._email.delete(0,END)
            self._email.insert(0,serv_cliente[6])
            self._telefono.delete(0,END)
            self._telefono.insert(0,serv_cliente[3])




if __name__ == '__main__':
    ventana = ThemedTk(themebg=True)
    ventana.set_theme(theme_name='ubuntu', themebg=True)
    aplicacion = ReservaDatosCliente(ventana)    
    ventana.mainloop()