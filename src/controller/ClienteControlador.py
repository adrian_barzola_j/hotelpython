from controller.ManejadorCursor import ManejadorCursor

class ClienteControlador:

    _SQL_INSERTAR = '''
        INSERT INTO cliente
           (identificacion
            ,nombre
            ,telefono
            ,direccion
            ,fecha_nacimiento
            ,email
        )
        VALUES
           (?,?,?,?,?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.cliente
        WHERE identificacion = COALESCE(?, identificacion)
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.cliente
        WHERE id_cliente = ?
    '''

    _SQL_CONSULTA_X_CED = '''
        SELECT * 
        FROM DBO.cliente
        WHERE identificacion = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE DBO.cliente SET
            identificascion = ?,
            nombre = =
        WHERE id_cliente = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.cliente WHERE id_cliente = ?    
    '''
    
    def insertar(self, cliente):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR,
                                    cliente._identificacion,
                                    cliente._nombre,
                                    cliente._telefono,
                                    cliente._direccion,
                                    cliente._fecha_nacimiento,
                                    cliente._email
            )
        except Exception as e:
            return e
    
    
    def actualizar(self, cliente):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ACTUALIZAR, 
                                    cliente._identificacion,
                                    cliente._nombre,
                                    cliente._id_cliente
            )
        except Exception as e:
            return e
    
    def eliminar(self, cliente):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    cliente._id_cliente
            )
        except Exception as e:
            return e
    
    def consultar_uno(self, id_cliente):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    id_cliente
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_identificacion(self, identificacion):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_CED,
                                    identificacion
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self, identificacion):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL, 
                                    identificacion
                                )
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []