from controller.ManejadorCursor import ManejadorCursor

class MascotaControlador:

    _SQL_INSERTAR = '''
        INSERT INTO reserva_mascota
           (id_reserva
           ,nombre
           ,tipo
           ,descripcion
           ,cuidados_especiales)
        VALUES
           (?,?,?,?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.reserva_mascota
        WHERE tipo = COALESCE(?, tipo)
        AND id_reserva = COALESCE(?, id_reserva)
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.reserva_mascota
        WHERE id_reserva_mascota = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE DBO.reserva_mascota SET
            NOMBRE = ?,
            tipo = ?
        WHERE id_reserva_mascota = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.reserva_mascota WHERE ID_reserva_mascota = ?    
    '''
    
    def insertar(self, mascota):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR, 
                                    mascota._id_reserva,
                                    mascota._nombre,
                                    mascota._tipo,
                                    mascota._descripcion,
                                    mascota._cuidados_especiales
            )
        except Exception as e:
            return e
    
    
    def actualizar(self, mascota):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ACTUALIZAR, 
                                    mascota._nombre,
                                    mascota._tipo,
                                    mascota._id_mascota
            )
        except Exception as e:
            return e
    
    def eliminar(self, mascota):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    mascota._id_mascota
            )
        except Exception as e:
            return e
    
    def consultar_uno(self, id_mascota):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    id_mascota
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self, id_reserva, tipo):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL, 
                                    id_reserva,
                                    tipo
                                )
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []