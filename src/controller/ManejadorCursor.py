import sys
from controller.DBService import DBService

# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance
# server = 'myserver,port' # to specify an alternate port
class ManejadorCursor:

    def __init__(self):
        self._cursor = None
        self._conexion = None
    
    def __enter__(self):
        print('Abriendo nuevo cursor')
        self._conexion = DBService.obtener_conexion()
        self._cursor = self._conexion.cursor()
        return self._cursor
    
    def __exit__(self, tipo_excepcion, valor_excepcion, detalle_excepcion):
        if valor_excepcion:
            self._conexion.rollback()
            print(f'Rollback de la transacción: {tipo_excepcion} - {valor_excepcion} - {detalle_excepcion}')
        else:
            self._conexion.commit()
            print('Commit de la transacción')
        self._cursor.close()
        self._conexion.close()

if __name__ == '__main__':
    cnxn = DBService.obtener_conexion()
    print(cnxn)
    DBService.cerrar()