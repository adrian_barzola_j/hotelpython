from controller.ManejadorCursor import ManejadorCursor

class TourControlador:

    _SQL_INSERTAR = '''
        INSERT INTO tour
           (nombre ,duracion ,costo ,estado ,resumen ,imagen_url)
        VALUES
           (?,?,?,?,?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.TOUR
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.TOUR
        WHERE ID_TOUR = ?
    '''

    _SQL_CONSULTA_X_Nm = '''
        SELECT * 
        FROM DBO.TOUR
        WHERE nombre = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE DBO.TOUR SET
            NOMBRE = ?,
            ESTADO = ?
        WHERE ID_TOUR = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.TOUR WHERE ID_TOUR = ?    
    '''
    
    def insertar(self, tour):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR, 
                                    tour._nombre,
                                    tour._duracion,
                                    tour._costo,
                                    tour._estado,
                                    tour._resumen,
                                    tour._imagen_url
            )
        except Exception as e:
            return e
    
    
    def actualizar(self, tour):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ACTUALIZAR, 
                                    tour._nombre,
                                    tour._duracion,
                                    tour._costo,
                                    tour._estado,
                                    tour._resumen,
                                    tour._imagen_url,
                                    tour._id_tour
            )
        except Exception as e:
            return e
    
    def eliminar(self, tour):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    tour._id_tour
            )
        except Exception as e:
            return e
    
    def consultar_uno(self, id_tour):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    id_tour
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_nombre(self, nombre):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_Nm,
                                    nombre
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL )
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []