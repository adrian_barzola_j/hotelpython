from controller.ManejadorCursor import ManejadorCursor

class TourDetalleControlador:

    _SQL_INSERTAR = '''
        INSERT INTO tour_detalle
           (id_tour, actividad)
        VALUES
           (?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.TOUR_DETALLE
        WHERE ID_TOUR = ?
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.TOUR_DETALLE
        WHERE ID_TOUR_DETALLE = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE DBO.TOUR SET
            ACTIVIDAD = ?
        WHERE ID_TOUR_DETALLE = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.TOUR_DETALLE WHERE ID_TOUR_DETALLE = ?    
    '''

    def instanciar_tour(self,
                id_tour_detalle = None,
                id_tour = None,
                actividad = None
            ):
        return TourDetalle(id_tour_detalle,
                           id_tour,
                           actividad
                        )
    
    def insertar(self, tour_detalle):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR, 
                                    tour_detalle._id_tour,
                                    tour_detalle._actividad
                                )
        except Exception as e:
            return e.message
    
    
    def actualizar(self, tour_detalle):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ACTUALIZAR, 
                                    tour_detalle._actividad,
                                    tour_detalle._id_tour_detalle
            )
        except Exception as e:
            return e.message
    
    def eliminar(self, tour_detalle):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    tour_detalle._id_tour_detalle
            )
        except Exception as e:
            return e.message
    
    def consultar_uno(self, id_tour_detalle):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    id_tour_detalle
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e.message)
            return None
    
    def consultar_muchos(self, id_tour):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL, id_tour )
                return conexion.fetchall()
        except Exception as e:
            print(e.message)
            return []