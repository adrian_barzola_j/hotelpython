from controller.ManejadorCursor import ManejadorCursor

class ReservaClienteControlador:

    _SQL_INSERTAR = '''
        INSERT INTO [dbo].[reserva_cliente]
           ([id_reserva]
           ,[id_cliente]
           ,[no_maletas]
           ,[fecha_ingreso]
           ,[fecha_salida]
           ,[observacion])
        VALUES
           (?,?,?,?,?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM [dbo].[reserva_cliente]
        WHERE id_reserva = COALESCE(?, id_reserva)
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM [dbo].[reserva_cliente]
        WHERE id_reserva_cliente = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE [dbo].[reserva_cliente] SET
            fecha_salida = ?,
            observacion = ?
        WHERE id_reserva_cliente = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE [dbo].[reserva_cliente] WHERE id_reserva_cliente = ?    
    '''
    
    def insertar(self, reserva_cliente):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR,
                                    reserva_cliente._id_reserva,
                                    reserva_cliente._id_cliente,
                                    reserva_cliente._no_maletas,
                                    reserva_cliente._fecha_ingreso,
                                    reserva_cliente._fecha_salida,
                                    reserva_cliente._observacion,
            )
        except Exception as e:
            return e
    
    
    def actualizar(self, reserva_cliente):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ACTUALIZAR, 
                                    reserva_cliente._fecha_salida,
                                    reserva_cliente._observacion
            )
        except Exception as e:
            return e
    
    def eliminar(self, reserva_cliente):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    reserva_cliente._id_reserva_cliente
            )
        except Exception as e:
            return e
    
    def consultar_uno(self, id_reserva_cliente):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    id_reserva_cliente
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self, id_reserva):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL, 
                                    id_reserva
                                )
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []