from controller.ManejadorCursor import ManejadorCursor

class ServicioAdicionalControlador:

    _SQL_INSERTAR = '''
        INSERT INTO servicio_adicional
           (descripcion
           ,costo
           ,membresia
           ,resumen
           ,imagen_url
           ,estado)
        VALUES
           (?,?,?,?,?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.SERVICIO_ADICIONAL
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.SERVICIO_ADICIONAL
        WHERE ID_SERVICIO_ADICIONAL = ?
    '''

    _SQL_CONSULTA_X_Nm = '''
        SELECT * 
        FROM DBO.SERVICIO_ADICIONAL
        WHERE descripcion = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE DBO.SERVICIO_ADICIONAL SET
            DESCRIPCION = ?,
            ESTADO = ?
        WHERE ID_SERVICIO_ADICIONAL = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.SERVICIO_ADICIONAL WHERE ID_SERVICIO_ADICIONAL = ?    
    '''
    
    def insertar(self, servicio_adicional):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR, 
                                    servicio_adicional._descripcion,
                                    servicio_adicional._costo,
                                    servicio_adicional._membresia,
                                    servicio_adicional._resumen,
                                    servicio_adicional._imagen_url,
                                    servicio_adicional._estado
            )
        except Exception as e:
            return e
    
    
    def actualizar(self, servicio_adicional):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ACTUALIZAR, 
                                    servicio_adicional._descripcion,
                                    servicio_adicional._estado,
                                    servicio_adicional._id_servicio_adicional
            )
        except Exception as e:
            return e
    
    def eliminar(self, servicio_adicional):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    servicio_adicional._id_servicio_adicional
            )
        except Exception as e:
            return e
    
    def consultar_uno(self, id_servicio_adicional):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    id_servicio_adicional
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_nombre(self, nombre_servicio_adicional):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_Nm,
                                    nombre_servicio_adicional
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL)
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []