from controller.ManejadorCursor import ManejadorCursor

class HabitacionControlador:

    _SQL_INSERTAR = '''
        INSERT INTO [dbo].[habitacion]
           ([tipo_habitacion]
           ,[numero_camas]
           ,[banio]
           ,[ventana]
           ,[aire_acondicionado]
           ,[ventilador]
           ,[recibidor]
           ,[armario]
           ,[minibar]
           ,[estado]
           ,[ultimo_hospedaje])
     VALUES
           (?,?,?,?,?,?,?,?,?,?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.HABITACION
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.HABITACION
        WHERE NUMERO_HABITACION = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE DBO.HABITACION SET
            ESTADO = ?
        WHERE NUMERO_HABITACION = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.HABITACION 
        WHERE NUMERO_HABITACION = ?
    '''
    
    def insertar(self, habitacion):
        pass
    
    
    def actualizar(self, habitacion):
        pass
    
    def eliminar(self, codigo_piso, no_habitacion):
        pass
    
    def consultar_uno(self, no_habitacion):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    no_habitacion
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL)
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []