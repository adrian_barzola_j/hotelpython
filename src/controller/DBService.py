import pyodbc as DB
import sys

# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance
# server = 'myserver,port' # to specify an alternate port
class DBService:

    _HOST = 'localhost' 
    _DATABASE = 'saturno' 
    _USERNAME = 'sa' 
    _PASSWORD = 'LeOnOr65' 
    _DB_PORT = '1433'
    _conexion = None 
    _cursor = None
    
    @classmethod
    def obtener_conexion(cls):
        try:
            # if cls._conexion is None:
            cls._conexion =  DB.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+cls._HOST+';DATABASE='+cls._DATABASE+';UID='+cls._USERNAME+';PWD='+ cls._PASSWORD)

            return cls._conexion
        except Exception as e:
            print(f'Ocurrió un error: {e}')
    
    @classmethod
    def obtener_cursor(cls):
        if cls._cursor is None:
            try:
                cls._cursor = cls.obtener_conexion().cursor()
                print(f'Cursor abierto correctamente: {cls._cursor}')
            except Exception as e:
                print(f'Ocurrió un error: {e}')
                sys.exit()
        return cls._cursor
    
    @classmethod
    def cerrar(cls):
        if cls._cursor:
            cls._cursor.close()
            cls._cursor = None
            print('Cursor cerrado')
        
        if cls._conexion:
            cls._conexion.close()
            cls._conexion = None
            print('Conexión cerrada')

if __name__ == '__main__':
    cnxn = DBService.obtener_conexion()
    print(cnxn)
    DBService.cerrar()