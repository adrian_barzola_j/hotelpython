from controller.ManejadorCursor import ManejadorCursor

class EquipajeControlador:

    _SQL_INSERTAR = '''
        INSERT INTO dbo.reserva_equipaje
           (id_reserva
           ,descripcion
           ,tipo
           ,valor_estimado
           ,resumen
           ,cuidados_especiales)
     VALUES
           (?,?,?,?,?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.reserva_equipaje
        WHERE id_reserva = COALESCE(?, id_reserva)
        AND descripcion = COALESCE(?, descripcion)
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.reserva_equipaje
        WHERE id_reserva_equipaje = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE DBO.reserva_equipaje SET
            descripcion = ?,
            resumen = ?
        WHERE id_reserva_equipaje = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.reserva_equipaje WHERE id_reserva_equipaje = ?    
    '''
    
    def insertar(self, equipaje):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR, 
                                    equipaje._id_reserva,
                                    equipaje._descripcion,
                                    equipaje._tipo,
                                    equipaje._valor_estimado,
                                    equipaje._resumen,
                                    equipaje._cuidados_especiales
            )
        except Exception as e:
            return e
    
    
    def actualizar(self, equipaje):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ACTUALIZAR, 
                                    equipaje._descripcion,
                                    equipaje._resumen,
                                    equipaje._id_equipaje
            )
        except Exception as e:
            return e
    
    def eliminar(self, equipaje):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    equipaje._id_equipaje
            )
        except Exception as e:
            return e
    
    def consultar_uno(self, id_equipaje):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    id_equipaje
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self, equipaje):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL, 
                                    equipaje._nombre,
                                    equipaje._estado
                                )
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []