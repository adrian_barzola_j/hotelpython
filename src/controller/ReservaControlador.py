from controller.ManejadorCursor import ManejadorCursor

class ReservaControlador:

    _SQL_INSERTAR = '''
       INSERT INTO [dbo].[reserva]
           ([id_cliente]
           ,[fecha_llegada]
           ,[fecha_salida]
           ,[no_adultos]
           ,[no_ninos]
           ,[nombre_tarjeta]
           ,[numero_tarjeta]
           ,[tipo_tarjeta]
           ,[fecha_expiracion]
           ,[cvv]
           ,[total_pagar]
           ,[estadi])
        VALUES
           (?,?,?,?,?,?,?,?,?,?,?,?)
        
        SELECT SCOPE_IDENTITY()
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.reserva
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.reserva
        WHERE ID_reserva = ?
    '''

    _SQL_ACTUALIZAR = '''
        
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.reserva WHERE ID_reserva = ?    
    '''
    
    def insertar(self, reserva):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR, 
                                    reserva._id_reserva,
                                    reserva._id_cliente,
                                    reserva._fecha_llegada,
                                    reserva._fecha_salida,
                                    reserva._no_adultos,
                                    reserva._no_ninos,
                                    reserva._nombre_tarjeta,
                                    reserva._numero_tarjeta,
                                    reserva._tipo_tarjeta,
                                    reserva._fecha_expiracion,
                                    reserva._cvv,
                                    reserva._total_pagar,
                                    reserva._estado
                                )
                return conexion.fetchone()
        except Exception as e:
            return e
    
    def actualizar(self, reserva):
        pass
    
    def eliminar(self, reserva):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    reserva._id_reserva
            )
        except Exception as e:
            return e
    
    def consultar_uno(self, id_reserva):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    id_reserva
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL )
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []