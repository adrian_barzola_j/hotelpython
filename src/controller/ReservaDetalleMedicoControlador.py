from controller.ManejadorCursor import ManejadorCursor

class ReservaDetalleMedicoControlador:

    _SQL_INSERTAR = '''
        INSERT INTO reserva_regmed
           (id_reserva
           ,id_cliente
           ,peso
           ,raza
           ,tipo_sangre
           ,sexo
           ,estatura
           ,enfermedades
           ,alergias
           ,condiciones_especiales)
        VALUES
           (?,?,?,?,?,?,?,?,?,?)
    '''

    _SQL_CONSULTA_GENERAL = '''
        SELECT * 
        FROM DBO.reserva_regmed
        WHERE id_reserva = COALESCE(?, id_reserva)
    '''

    _SQL_CONSULTA_X_ID = '''
        SELECT * 
        FROM DBO.reserva_regmed
        WHERE id_regmed = ?
    '''

    _SQL_ACTUALIZAR = '''
        UPDATE DBO.reserva_regmed SET
            id_cliente = ?
        WHERE id_regmed = ?
    '''

    _SQL_ELIMINAR = '''
        DELETE DBO.reserva_regmed WHERE id_regmed = ?    
    '''
    
    def insertar(self, detalle_medico):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_INSERTAR,
                                    detalle_medico._id_reserva,
                                    detalle_medico._id_cliente,
                                    detalle_medico._peso,
                                    detalle_medico._raza,
                                    detalle_medico._tipo_sangre,
                                    detalle_medico._sexo,
                                    detalle_medico._estatura,
                                    detalle_medico._enfermedades,
                                    detalle_medico._alergias,
                                    detalle_medico._condiciones_especiales
            )
        except Exception as e:
            return e
    
    
    def actualizar(self, detalle_medico):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ACTUALIZAR, 
                                    detalle_medico._id_cliente,
                                    detalle_medico._id_reserva_detalle_medico
            )
        except Exception as e:
            return e
    
    def eliminar(self, detalle_medico):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_ELIMINAR,
                                    detalle_medico._id_reserva_detalle_medico
            )
        except Exception as e:
            return e
    
    def consultar_uno(self, _id_reserva_detalle_medico):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_X_ID,
                                    _id_reserva_detalle_medico
                        )
                return conexion.fetchone()

        except Exception as e:
            print(e)
            return None
    
    def consultar_muchos(self, id_reserva):
        try:
            with ManejadorCursor() as conexion:
                conexion.execute(self._SQL_CONSULTA_GENERAL, 
                                    id_reserva
                                )
                return conexion.fetchall()
        except Exception as e:
            print(e)
            return []